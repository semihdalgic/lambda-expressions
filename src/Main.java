import functional.custom.ICustomPredicate;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static final int MIN_LENGTH = 6;
    public static final int MAX_LENGTH = 8;

    public static void main(String[] args) {

        //#BEGIN Define functional interface behaviors

        //Usual way to create lambda notation
        ICustomPredicate isEmptyLambda = input -> input.isEmpty();

        //You can specify parameter type, in this case actually no need to use
        ICustomPredicate isEmptyLambdaWithParamType = (String input) -> input.isEmpty();

        //If you need multiple lines in body, you can use curly braces. If you use braces, be careful because you must use return keyword
        ICustomPredicate isEmptyLambdaMultiLineBody = input -> {
            if (input == null)
                return true;
            else
                return input.isEmpty();
        };

        //You can use factory methods in lambdas. It automatically takes the parameter and uses it.
        ICustomPredicate isEmptyLambdaWithFactory = String::isEmpty;

        //This is the way that we used to.
        ICustomPredicate isEmptyOldWay = new ICustomPredicate() {
            @Override
            public boolean test(String input) {
                return input.isEmpty();
            }
        };

        //#END

        //#BEGIN These are the calls with behaviours defined above.
        printPredicate("Functional", (input) -> input.isEmpty());

        printPredicate("Functional", isEmptyLambda);

        printPredicate("Functional", isEmptyLambdaWithParamType);

        printPredicate("Functional", isEmptyLambdaMultiLineBody);

        printPredicate("Functional", isEmptyLambdaWithFactory);

        printPredicate("Functional", isEmptyOldWay);

        printPredicate("Functional", new ICustomPredicate() {
            @Override
            public boolean test(String input) {
                return input.isEmpty();
            }
        });
        //#END


        //#BEGIN Let's try different behaviours on the same functional interface
        ICustomPredicate validateMinLenth = input -> input.length() > MIN_LENGTH;
        ICustomPredicate validateMaxLenth = input -> input.length() < MAX_LENGTH;

        List<ICustomPredicate> rules = new ArrayList<>();
        rules.add(validateMinLenth); // or rules.add(input.length() > MIN_LENGTH);
        rules.add(validateMaxLenth); // or rules.add(input.length() > MAX_LENGTH);

        System.out.println("Validation Starts: ");
        for (ICustomPredicate rule : rules) {
            printPredicate("Functional", rule);
        }
        //#END

    }


    public static void printPredicate(String input, ICustomPredicate validator) {
        boolean result = validator.test(input);
        System.out.println("printPredicate: " + result);
    }

}
