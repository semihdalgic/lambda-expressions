package functional.custom;

/**
 * Created by semihd on 8.03.2017.
 */
@FunctionalInterface
public interface ICustomUnaryOperator {

    String apply(String input);
}
