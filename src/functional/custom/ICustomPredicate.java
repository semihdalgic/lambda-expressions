package functional.custom;

/**
 * Created by semihd on 8.03.2017.
 */
public interface ICustomPredicate {

    boolean test(String input);
}
