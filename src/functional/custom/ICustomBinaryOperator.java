package functional.custom;

/**
 * Created by semihd on 8.03.2017.
 */
@FunctionalInterface
public interface ICustomBinaryOperator {

    int apply(int a, int b);
}
