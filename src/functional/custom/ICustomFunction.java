package functional.custom;

/**
 * Created by semihd on 8.03.2017.
 */
public interface ICustomFunction {

    String apply(int a);
}
